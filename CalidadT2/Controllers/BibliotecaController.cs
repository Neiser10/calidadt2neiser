﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Constantes;
using CalidadT2.Models;
using CalidadT2.Repository;
using CalidadT2.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    [Authorize]
    public class BibliotecaController : Controller
    {
        
        private readonly IBibliotecaRepository _biblioteca;
        private readonly ICookieAuthService _cookieAuthService;
        private readonly IUsuarioRepository _usuario;
        public BibliotecaController(IBibliotecaRepository _biblioteca,ICookieAuthService _cookieAuthService,IUsuarioRepository _usuario)
        {
            this._usuario = _usuario;
           
            this._biblioteca = _biblioteca;
            this._cookieAuthService = _cookieAuthService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            Usuario user = LoggedUser();

            var model=_biblioteca.MiBiblioteca(user);

            return View(model);
        }

        [HttpGet]
        public ActionResult Add(int libro)
        {
            Usuario user = LoggedUser();

           _biblioteca.Agregar(user,libro);

           ModelState.AddModelError("SuccessMessage" , "Se añádio el libro a su biblioteca");
            

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult MarcarComoLeyendo(int libroId)
        {
            Usuario user = LoggedUser();

            _biblioteca.MarcarComoLeyendo(user,libroId);

            ModelState.AddModelError("SuccessMessage" ,"Se marco como leyendo el libro");

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult MarcarComoTerminado(int libroId)
        {
            Usuario user = LoggedUser();

           _biblioteca.MarcarComoTerminada(user,libroId);

           ModelState.AddModelError("SuccessMessage" , "Se marco como leyendo el libro");

            return RedirectToAction("Index");
        }

        private Usuario LoggedUser()
        {
        
            _cookieAuthService.SetHttpContext(HttpContext);
            var claim = _cookieAuthService.ObetenerClaim();
            var user = _usuario.UsuarioLogeado(claim);
            return user;
        }
    }
}
