﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CalidadT2.Models;
using CalidadT2.Repository;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    public class HomeController : Controller
    {
        private AppBibliotecaContext app;
        private readonly ILibroRepository _libro;
        public HomeController(AppBibliotecaContext app,ILibroRepository _libro)
        {
            this.app = app;
            this._libro = _libro;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = _libro.TodosLOsLibros();
            return View(model);
        }
    }
}
