using System.Linq;
using System.Security.Claims;
using CalidadT2.Models;

namespace CalidadT2.Repository
{
    public interface IUsuarioRepository
    {
        public Usuario EncontrarUsuario(string username, string password);
        public Usuario UsuarioLogeado(Claim claim);
    }
    public class UsuarioRepository : IUsuarioRepository
    {
        private AppBibliotecaContext context;

        public UsuarioRepository(AppBibliotecaContext context)
        {
            this.context = context;
        }

        public Usuario EncontrarUsuario(string username, string password)
        {
            var usuario =context.Usuarios.FirstOrDefault(o => o.Username == username && o.Password == password);
            return usuario;
        }

        public Usuario UsuarioLogeado(Claim claim)
        {
            var user = context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }
    }
}